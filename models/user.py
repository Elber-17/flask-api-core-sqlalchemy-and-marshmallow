from sqlalchemy import Column
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import TIMESTAMP
from sqlalchemy.orm import validates

from core.sqlalchemy import db


class User(db.Model):
	id = Column(INTEGER(unsigned=True), primary_key=True)
	name = Column(VARCHAR(256))
	password = Column(VARCHAR(1024))
	user_status_id = Column(TINYINT(), db.ForeignKey('user_status.id'))
	creation_date = Column(TIMESTAMP())
	last_modification = Column(TIMESTAMP())
	
	user_status = db.relationship('UserStatus')

class UserStatus(db.Model):
    id = Column(TINYINT(), primary_key=True)
    code = Column(VARCHAR(32))
    description = Column(VARCHAR(128))