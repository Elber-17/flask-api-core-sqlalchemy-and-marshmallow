from importlib import import_module
from flask import request
from marshmallow import Schema
from marshmallow import fields
from marshmallow import EXCLUDE
from marshmallow import post_load
from sqlalchemy import desc
from sqlalchemy import asc
from marshmallow import ValidationError
from sqlalchemy_filters import apply_filters
from orderdict import order
from flask_jwt_extended import jwt_required

from core.sqlalchemy import db
from constants.limit import previous_limit
from handlers.schema_handler import SchemaErrorHandler

fields.Field.default_error_messages = {
	'null': 'Este campo no puede estar vacio.',
	'required': 'Este campo no puede estar vacio.',
	'validator_failed': 'Invalid value.'
	}

fields.Number.default_error_messages = {
	'invalid': 'No es un numero valido.',
	'too_large': 'Number too large.'
	}

fields.Integer.default_error_messages = {
	"invalid": "No es un número valido."
	}

class BaseSchemas(Schema):
	class Meta:
		ordered = True
	 
	error_messages = {
        "unknown": "Este campo no existe",
        "type": "Custom invalid type error message.",
    }

	response = {
					'count' : 0,
					'offset' : 0,
					'limit' : 10,
					'next_page' : '',
					'previous_page' : ''
				}

	#@jwt_required
	def get(self, get_args=None, id=False, raw_result=False):
		get_args = get_args or dict(request.args.copy())
		_model_ = self.__get_model_with_filtered_fields__(get_args)

		return _model_.__get__(get_args=get_args, id=id, raw_result=raw_result)

	@classmethod
	def __get_model_with_filtered_fields__(cls, get_args):
		from marshmallow.fields import Nested

		exclude = []
		_model_ = cls()

		for key, value in get_args.items():
				if key == 'fields':
					if not value:
						return {'Bad Request' : 'El campo \'fields\' no puede estar vacio'}, 400

					counter = 0
					only = value.split(',')

					for elem in only:
						try:
							if type(_model_.fields[only[counter][:-3]]) == Nested:
								only[counter] = only[counter][:-3]

							counter += 1

						except:
							counter += 1
							continue

					exclude = [elem for elem in list(_model_.fields) if elem not in only]
					
					if len(exclude) == len(_model_.fields):
						return {'Bad Request' : 'Parece que tienes errores en los campos que quieres filtrar'}, 400

					del get_args['fields']

					break

		return cls(exclude=exclude)
	
	def __get__(self, get_args=None, id=False, raw_result=False):
		self.many = True
		# get_args = get_args if get_args != None else dict(request.args.copy())
		response = self.response.copy()
		next_page = request.base_url
		previous_page = request.base_url
		page = 1
		get_args, response = self.__set_response_limit_and_offset__(get_args, response)
		page = (response['offset'] / response['limit']) + 1
		query = self.__model__.query if not id else self.__filter_by_id__(id)
		query = query if not get_args else self.__apply_filters__(query, get_args)

		if type(query) == tuple:
			return query
			
		result_object = query.paginate(page, response['limit'])
		result_dump = result_object.items
		result_dump = self.dump(result_dump)

		if not result_dump:
			return {'Not Found' : 'No se encontraron resultados'}, 404

		if raw_result:
			return result_dump[0], 200
		
		return self.__get_response__(response, result_object, result_dump, get_args), 200
	
	def __set_response_limit_and_offset__(self, get_args, response):
		if get_args.get('limit'):
			response['limit'] = int(get_args['limit'])

			if not response['limit']:
				response['limit'] = self.response['limit']

			del get_args['limit']

		if get_args.get('offset'):
			response['offset'] = int(get_args['offset'])

			if response['offset'] == 1 or response['offset'] == 2:
				response['offset'] += 0.5

			del get_args['offset']
		
		return get_args, response
	
	def __filter_by_id__(self, id):
		primary_key = self.get_primary_key(False)
		query = self.__model__.query

		return self.__add_filters__(query, {primary_key : id})
	
	def get_primary_key(self, model):
		if not model:
			primary_key = list(self.__model__.__table__.c)[0]

		else:
			primary_key = list(model.__table__.c)[0]

		primary_key = str(primary_key)
		index = primary_key.index('.')
		primary_key = primary_key[index+1:]

		return primary_key
	
	def __add_filters__(self, query, args):
		filters = []
		args = self.__search_id_query_string__(args)

		for key, value in args.items():
			_filter_ = {}

			if key == 'order' or key == 'desc' or key == 'asc':
				if key == 'desc' or key == 'asc':
					continue

				if value not in self.__filtering__:
					raise Exception("No se puede ordenar la busqueda por el campo '{value}', no existe.".format(value=value))

				if args.get('desc', False):
					query = query.order_by(desc(value))

				elif args.get('asc', False):
					query = query.order_by(asc(value))
				
				else:
					query = query.order_by(value)

			elif key not in self.__filtering__:
				raise Exception("No se puede filtrar la busqueda por el campo '{key}', está vacío o no existe".format(key=key))

			else:
				_filter_['field'] = key
				_filter_['op'] = 'like'
				_filter_['value'] = '%' + value + '%'
				filters.append(_filter_)

		filtered_query = apply_filters(query, filters)

		return filtered_query
	
	def __apply_filters__(self, query, get_args):
		try:
			query = self.__add_filters__(query, get_args)
			return query

		except Exception as msg:
			return {'Bad Request' : str(msg)}, 400
	
	def __search_id_query_string__(self, args):
		entities = []
		filters = [{'or' : []}]
		query = None
		result = None
		primary_key = None

		for key in args.keys():
			if key == 'id':
				continue
			if key[-2:] == 'id':
				entities.append(key[:-3])
			
		if not entities:
			return args
		
		for entity in entities:
			key = entity + '_id'
			value = args[key]
			path = self.__contruct_module_path__(entity)
			class_name = self.__get_module_class_name__(path, entity)
			entity = self.load_module(path, class_name)
			query = entity.query
			atributes = [atribute for atribute in dir(entity) if atribute[:2] != '__' and atribute[:1] != '_']
			atributes = [atribute for atribute in atributes if 'metadata' not in atribute and 'query' not in atribute and 'query_class' not in atribute]
		
			for atribute in atributes:
				_filter_ = {}
				_filter_['field'] = atribute
				_filter_['op'] = 'like'
				_filter_['value'] = '%' + value + '%'
				filters[0]['or'].append(_filter_)

			result = apply_filters(query, filters).first()
			primary_key = self.get_primary_key(result)
			args[key] = str(getattr(result, primary_key))

		return args
	
	def __contruct_module_path__(self, entity):
		from os.path import exists, join

		path = join('models', entity)

		if not exists(path + '.py'):
			for underscore in range(entity.count('_')):
				entity = entity[::-1][entity[::-1].find('_')+1:][::-1]
				path = join('models', entity)
				if exists(path + '.py'):
					return path

		return False

	def __get_module_class_name__(self, path, entity):
		class_name = entity.replace('_', ' ').title().replace(' ', '')
		module = import_module(path.replace('/', '.'))
		if getattr(module, class_name, False):
			return class_name
		
	def load_module(self, path, classname, *args):
		module = import_module(path.replace('/', '.'))
		_class_ = getattr(module, classname)
		if args:
			return _class_(*args)
		else:
			return _class_()
	
	def __get_response__(self, response, result_object, result_dump, get_args):
		_response_ = {}
		_response_['offset'] = int(response['offset'])
		_response_['count'] = result_object.total
		_response_['next_page'] = self.__next_page__(result_object, response['offset'], response['limit'], get_args)
		_response_['previous_page'] = self.__previous_page__(result_object, response['offset'], response['limit'], get_args)
		_response_['data'] = [order(list(self.fields),elem) for elem in result_dump]

		return _response_

	def __next_page__(self, result, offset, limit, args):
		if len(result.next().items) == 0:
			return ''

		count = result.total
		offset = offset + limit
		
		if offset + limit > count:
			previous_limit[0] = limit

		while offset + limit > count:
			limit -= 1

		url = '?offset=' + str(offset) + '&limit=' + str(limit)

		if args:
			for key, value in args.items():
				url += '&{key}={value}'.format(key=key, value=value)

		return url

	def __previous_page__(self, result, offset, limit, args):
		if not result.has_prev:
			return ''

		if previous_limit[0]:
			limit = previous_limit[0]

		offset = offset - limit
		
		while offset < 0:
			offset += 1

		url = '?offset=' + str(offset) + '&limit=' + str(limit)

		if args:
			for key, value in args.items():
				url += '&{key}={value}'.format(key=key, value=value)

		return url

	#@jwt_required
	def post(self, post_args=False, show_url=False):
		args = post_args or dict(request.args.copy())

		insert_many = args.get('insert_many', False)
		insert_many_for = ''
		insert_many_values = []

		if insert_many:
			insert_many_for = insert_many[:insert_many.index(':')]
			insert_many_values = insert_many[insert_many.index(':')+1:].split(',')
			insert_many = {'insert_many_for' : insert_many_for, 'insert_many_values' : insert_many_values}
			del args['insert_many']
			
		
		for key in args.keys():
			if key in self.fields:
				pass
			else:
				return {'Bad Request' : "No existe el parametro '{key}' para esta entidad".format(key=key)}, 400
		
		primary_key = self.get_primary_key(False)

		if not primary_key in args:
			args[primary_key] = self.get_next_id()
	
		status = self.post_insert(primary_key, args, insert_many)

		if(type(status) == tuple):
			return status

		return self.post_response(primary_key, args, show_url)

	def get_next_id(self):
		from sqlalchemy import func
		from sqlalchemy import select
		
		primary_key = list(self.__model__.__table__.c)
		result =  db.session.execute(select([func.max(primary_key[0])])).first()[0]

		if not result:
			return 1

		result += 1
		
		return result	
	
	def post_insert(self, primary_key, args, insert_many=False):
		response_many = []

		if not insert_many:
			try:
				entity = self.load(args)

			except ValidationError as error:
				return {'Bad Request' : error.messages}, 400

			db.session.add(entity)

			try:
				db.session.commit()

			except Exception as msg:
				handler = SchemaErrorHandler(msg)

				return handler.response(default=False)

			return True
		
		for data in insert_many['insert_many_values']:
			try:
				args[insert_many['insert_many_for']] = data
				entity = self.load(args)

			except ValidationError as error:
				return {'Bad Request' : error.messages}, 400

			db.session.add(entity)

			try:
				db.session.commit()

			except Exception as msg:
				handler = SchemaErrorHandler(msg)

				return handler.response(default=False)

			response_many.append(self.post_response(primary_key, args, False)[0])

		return response_many, 201
	
	def post_response(self, primary_key, args, show_url):
		response = {}
		response['Status'] = 'Success'
		response[primary_key] = args[primary_key]

		if show_url:
			response['url'] = request.base_url + str(args[primary_key]) + '/'

		#del args[primary_key]
		_order = list(self.fields)
		_order.insert(0, 'Status')
		_order.insert(1, primary_key)

		if show_url:
			_order.append('url')

		for key, value in args.items():
			response[key] = value

		response = order(_order, response)

		return response, 201		

	#@jwt_required
	def put(self, args=False, response=True, construct_url=True, show_url=False, patch=False):
		args = args or dict(request.args.copy())
		_response = {}

		if len(args) <= 1:
			return {'Bad Request' : 'Debes mandar los parametros que quieres actualizar y/o el identificador de la entidad'}, 400

		primary_key = self.get_primary_key(False)

		if not primary_key in args:
			return {'Bad Request' : 'Falta la clave primaria (id) de la entidad para actualizar'}, 400

		for key in args.keys():
			if key in self.fields:
				pass
			else:
				return {'Bad Request' : "No existe el parametro '{key}' para esta entidad".format(key=key)}, 400

		query = self.__model__.query
		query = self.__add_filters__(query, {primary_key : args[primary_key]})
		entity = query.first()

		if not entity:
			return {'Bad Request' : 'No existe una entidad con ese identificador (id)'}, 400

		entity_to_update = dict(self.dump(entity))

		for key, value in args.items():
			try:
				if value == "" or value.strip() == "":
					args[key] = None
			except:
				continue

		for key, value in args.items():
			try:
				entity_to_update[key] = value
			except Exception:
				msg = 'el parametro \'{key}\' no existe para esta entidad'.format(key=key)
				handler = SchemaErrorHandler(msg)
				return handler.response(default=False)

		if patch:
			partial = tuple([elem for elem in entity_to_update.keys() if elem not in [elem for elem in args.keys()]])

			for key in entity_to_update.copy().keys():
				if key in partial:
					del entity_to_update[key]

		try:
			if patch:
				entity_to_update = self.load(entity_to_update, partial=partial)

			else:
				entity_to_update = self.load(entity_to_update, unknown=EXCLUDE)

		except ValidationError as error:
			return {'Bad Request' : error.messages}, 400
			
		if not patch:
			for elem in self.dump(entity_to_update).keys():
				if elem not in args.keys() and not self.fields[elem].dump_only:
					return {'Bad Request' : 'Faltan parametros'}, 400

		for key, value in self.dump(entity_to_update).copy().items():
			if value == None and args.get(key, False) == None:
				setattr(entity, key, value)

			elif value != None:
				setattr(entity, key, value)

		db.session.add(entity)

		try:
			db.session.commit()
		except Exception as msg:
			handler = SchemaErrorHandler(msg)
			return handler.response()

		if not response:
			return True

		_response['Status'] = 'Success'
		_response[primary_key] = args[primary_key]

		if show_url:
			if not construct_url:
				_response['url'] = request.base_url
				
			else:
				_response['url'] = request.base_url + str(args[primary_key]) + '/'

		del args[primary_key]
		_order = list(self.fields)
		_order.insert(0, 'Status')
		_order.insert(1, primary_key)

		if show_url:
			_order.append('url')

		for key, value in args.items():
			_response[key] = value

		_response = order(_order, _response)

		return _response, 200

	#@jwt_required
	def patch(self, *args, **kwargs):
		return self.put(patch=True,*args,**kwargs)
	
	@jwt_required
	def delete(self, args=False):
		args = args or dict(request.args.copy())

		primary_key = self.get_primary_key(False)

		if not primary_key in args:
			return {'Bad Request' : 'Falta la clave primaria (id) de la entidad para eliminar'}, 400

		query = self.__model__.query
		query = self.__add_filters__(query, {primary_key : args[primary_key]})
		entity = query.first()
		
		if not entity:
			return {'Bad Request' : 'No existe una entidad con ese identificador (id)'}, 400

		db.session.delete(entity)

		try:
			db.session.commit()
		except Exception as msg:
			handler = SchemaErrorHandler(msg)
			return handler.response(default=False)

		return  'No Content', 204

	@post_load
	def make_object(self, data, **kwargs):
		return self.__model__(**data)