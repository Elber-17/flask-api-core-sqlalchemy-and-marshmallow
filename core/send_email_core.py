from flask import url_for
from email.message import Message
from smtplib import SMTP
from itsdangerous import URLSafeTimedSerializer
from threading import Thread

from time import strftime

from templates.email import view

def send_email(message_confirm_email, password):
    server = SMTP('smtp.gmail.com: 587')
    server.starttls()
    server.login(message_confirm_email['From'], password)
    server.sendmail(message_confirm_email['From'], message_confirm_email['To'], message_confirm_email.as_string().encode('utf-8'))
    server.quit();print(strftime('%H:%M:%S ') + 'listo')

def construct_email(email):
    url_safe_timed_serializer = URLSafeTimedSerializer('super-secret') # cambiar esto también
    token_url = url_safe_timed_serializer.dumps(email, salt='email-confirm')
    url = url_for('confirm_email', token=token_url, _external=True)
    body = view.format(link=url)
    message_confirm_email = Message()
    password = '16727897'
    message_confirm_email['From'] = 'elbernava11@gmail.com'
    message_confirm_email['To'] = email
    message_confirm_email['Subject'] = 'probando ronda 2'
    message_confirm_email.add_header('Content-Type', 'text/html')
    message_confirm_email.set_payload(body)
    thread = Thread(target=send_email, args=(message_confirm_email, password,))
    thread.start()

def send_link_confirmation(email):
    construct_email(email)