from itsdangerous import URLSafeTimedSerializer
from itsdangerous import SignatureExpired

def confirm_token_email(token):
    try:
        url_safe_timed_serializer = URLSafeTimedSerializer('super-secret') # cambiar esto también
        email = url_safe_timed_serializer.loads(token, salt='email-confirm', max_age=(15))

        return 'email confirmado : ' + email

    except SignatureExpired:
        return 'token expirado'