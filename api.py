from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request
from flask_jwt_extended import JWTManager
from flask_cors import CORS

from core.sqlalchemy import db
from config.db_config import URI
from core.marshmallow import ma
from resources.login import Login
from helpers.confirm_email import confirm_token_email 
from resources.user import User
from resources.user import UserStatus
from resources.registry import Registry

from resources.logout import LogOut
from handlers.token_handler import TokenErrorHandler

app = Flask(__name__)
# app.config['SERVER_NAME'] = '192.168.8.102'
# app.config['SERVER_PORT'] = 17499
app.config['JSON_SORT_KEYS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 60 * 20
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']

app.config['TEMPLATES_AUTO_RELOAD'] = True
# app.config['SQLALCHEMY_ECHO'] = True

jwt = JWTManager(app)
db.init_app(app)
ma.init_app(app)
blacklist = set()
CORS(app)

log_out = LogOut(blacklist)
jwt.token_in_blacklist_loader(log_out.check_if_token_in_blacklist)
jwt.revoked_token_loader(TokenErrorHandler.revoken_token)
jwt.unauthorized_loader(TokenErrorHandler.unauthorized_token)
jwt.expired_token_loader(TokenErrorHandler.expired_token)

""" RUTA DE LA DOCUMENTACION DE LA API """

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

""" RUTA PARA CONFIRMAR EL EMAIL """

@app.route('/confirm-email/<token>/', methods=['GET'])
def confirm_email(token):
    return confirm_token_email(token)

""" RUTA DEL LOGIN """

@app.route('/login/', methods=['POST'])
def login():
    login = Login()

    return login.response()

""" RUTAS DEL MODULO USER """

@app.route('/user/', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
def user():
    user = User()

    return user.response()


@app.route('/user/status/', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
def user_status():
    user_status = UserStatus()

    return user_status.response()


@app.route('/registry-user/', methods=['POST'])
def registry_user():
    registry = Registry()

    return registry.user_registry()

""" RUTA DEL LOG-OUT """

@app.route('/log-out/', methods=['DELETE'])
def logout():
    return log_out.logout()

@app.errorhandler(404)
def not_found_error(error):
    return jsonify(Error='Parece que tienes errores en los parametros de busqueda o la url indicada no existe'), 400

if __name__ == "__main__":
	app.run(use_debugger=True, host= "localhost", port=17499, use_reloader=True)