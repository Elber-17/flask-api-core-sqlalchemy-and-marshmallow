from flask import request
from marshmallow import ValidationError

from core.resource_core import BaseResource
from schemas.user import User
from handlers.schema_handler import SchemaErrorHandler
from core.sqlalchemy import db

class Registry(BaseResource):
    def user_registry(self):
        if not 'name' in request.args:
            return self.make_response({'Bad Request' : 'Falta el parametro name'}, 400)

        if not 'password' in request.args:
            return self.make_response({'Bad Request' : 'Falta el parametro password'}, 400)
        
        schema = User()
        request_args = dict(request.args.copy())
        id = schema.get_next_id()
        request_args[schema.get_primary_key(False)] = id
        request_args['user_status_id'] = 1
        response = {}

        try:
            user = schema.load(request_args)

        except ValidationError as error:
            return {'Bad Request' : error.messages}, 400

        db.session.add(user)

        try:
            db.session.commit()

        except Exception as msg:
            handler = SchemaErrorHandler(msg)

            return handler.response(default=False)
        

        response['Status'] = 'Success'
        response[schema.get_primary_key(False)] = id
        
        for key, value in request_args.items():
            response[key] = value

        return self.make_response(response, 201)