from flask import request
# from core.send_email_core import send_link_confirmation
# from threading import Thread
# from core.send_email_core import send_link_confirmation

from core.resource_core import BaseResource
from schemas.user import User as SchemaUser
from schemas.user import UserStatus as SchemaUserStatus


class User(BaseResource):
	header = {'Allow' : 'GET, POST, PUT, PATCH, DELETE'}
	
	def response(self, id=False):
		if request.method == 'GET':
			user = SchemaUser()
			response, code = user.get(get_args=dict(request.args.copy()), id=id)
			
		elif request.method == 'POST':
			from datetime import datetime

			user = SchemaUser()
			post_args = dict(request.args.copy())
			post_args['user_status_id'] = 1
			# post_args['creation_date'] = str(datetime.now().strftime('%d/%m/%Y %I:%M:%S %p'))
			response, code = user.post(post_args=post_args, show_url=True)

			# if code == 201:
			# 	send_link_confirmation(post_args['email'])

		elif request.method == 'PUT':
			user = SchemaUser()
			response, code = user.put()
		
		elif request.method == 'PATCH':
			user = SchemaUser()
			response, code = user.patch()

		elif request.method == 'DELETE':
			user = SchemaUser()
			response, code = user.delete()

		return self.make_response(response, code, self.header)

class UserStatus(BaseResource):
	header = {'Allow' : 'GET, POST, PUT, PATCH, DELETE'}

	def response(self, id=False):
		if request.method == 'GET':
			candidate_status = SchemaUserStatus()
			response, code = candidate_status.get(get_args=dict(request.args.copy()), id=id)
			
		elif request.method == 'POST':
			candidate_status = SchemaUserStatus()
			response, code = candidate_status.post()

		elif request.method == 'PUT':
			candidate_status = SchemaUserStatus()
			response, code = candidate_status.put()
		
		elif request.method == 'PATCH':
			candidate_status = SchemaUserStatus()
			response, code = candidate_status.patch()

		elif request.method == 'DELETE':
			candidate_status = SchemaUserStatus()
			response, code = candidate_status.delete()

		return self.make_response(response, code, self.header)