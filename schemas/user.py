from marshmallow import validates
from marshmallow import ValidationError
from marshmallow import validates_schema

from core.schemas_core import BaseSchemas
from core.schemas_core import fields
from models.user import User as ModelUser
from models.user import UserStatus as ModelUserStatus


class User(BaseSchemas):
	__model__ = ModelUser
    
	id = fields.Integer()
	name = fields.String(required=True)
	password = fields.String(required=True)
	user_status_id = fields.Integer(load_only=True)
	creation_date = fields.DateTime(format='%d/%m/%Y %I:%M:%S %p')
	last_modification = fields.DateTime(format='%d/%m/%Y %I:%M:%S %p')
	
	user_status = fields.Nested('UserStatus', data_key='user_status_id', dump_only=True)
	
	__filtering__ = ['id', 'name', 'password', 'user_status_id', 'creation_date', 'last_modification']
	
	# @validates('email')
	# def validate_email(self, email):
	# 	if '@' not in  email or '.com' not in email:
	# 		raise ValidationError('Por favor ingresa un dirección de correo electronica válida')

class UserStatus(BaseSchemas):
	__model__ = ModelUserStatus
	
	id = fields.Integer()
	code = fields.String(required=True)
	description = fields.String(required=True)

	__filtering__ = ['id', 'code', 'description']